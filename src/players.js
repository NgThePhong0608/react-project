import messi from './images/messi.jpg';
import ronaldo from './images/ronaldo.jpg';
import mbape from './images/mbape.jpg';

export const players = [
  {
    player_name: "Lionel Messi",
    describe: "The G.O.A.T of Football",
    img: messi,
    id: 1,
  },
  {
    player_name: "Cristiano Ronaldo",
    describe: "The G.O.A.T of Portugal Football",
    img: ronaldo,
    id: 2,
  },
  {
    player_name: "Kylian Mbape",
    describe: "The G.O.A.T of France Football",
    img: mbape,
    id: 3,
  },
];
