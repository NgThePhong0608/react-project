import React from "react";
import ReactDOM from "react-dom/client";

import "./index.css";

import { books } from "./books";
import Book from "./Book";

import { players } from "./players";
import Player from "./Player";

function BookList() {
  return (
    <>
      <h1>amazon best sellers</h1>
      <section className="booklist">
        {books.map((book) => {
          return <Book {...book} key={book.id} />;
        })}
      </section>
    </>
  );
}

function PlayerList() {
  return (
    <>
      <h1>3 best football players in the word</h1>
      <section className="booklist">
        {players.map((player) => {
          return <Player {...player} key={player.id} />;
        })}
      </section>
    </>
  );
}

const root = ReactDOM.createRoot(document.getElementById("root"));

// root.render(<BookList />);
root.render(<PlayerList />);
