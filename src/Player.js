const Player = (props) => {
    const { player_name, describe, img } = props;

    return (
        <>
            <article className='book'>
                <img src={img} alt={player_name} />
                <h2>{player_name}</h2>
                <h4>{describe} </h4>
            </article>
        </>
    )
}

export default Player;